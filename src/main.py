import requests
from prometheus_client import  start_http_server, Gauge, REGISTRY
from requests.auth import HTTPBasicAuth
from os import environ
import psutil

ip = environ['ip']

def collect_users():
        total = requests.get(f'http://{ip}:6610/api/users?offset=0&count=100',auth=HTTPBasicAuth('admin', 'admin'))
        users = 0
        for user in total.json():
                users += 1
        return users

users_onedev = Gauge("number_of_users", "Number_Of_Users")      
users_onedev.set_function(collect_users)

def get_data_projects():

        res = requests.get(f'http://{ip}:6610/api/projects?offset=0&count=100', auth=HTTPBasicAuth('admin', 'admin'))

        projects = 0
        for project in res.json():
                projects += 1

        return projects

metric = Gauge("number_of_projects", "Number_Of_Projects")
metric.set_function(get_data_projects)

def get_data_virtual_memory():

        virtual_memory = psutil.virtual_memory().percent
        print(virtual_memory)
        return virtual_memory

metric_cpu = Gauge("virtual_memory_percent", "Virtual_Memory_Percent")
metric_cpu.set_function(get_data_virtual_memory)

def passed_b():

    """count passed build by authentificating os."""

    result = requests.get(f'http://{ip}:6610/api/builds?offset=0&count=100', auth=HTTPBasicAuth('admin', 'admin'))
    succeeded_builds = 0
    for job in result.json():
        if job['status'] == "SUCCESSFUL":
            succeeded_builds += 1
            print(succeeded_builds)
    return succeeded_builds


passed = Gauge("succeeded_builds", "Succeeded builds")
passed.set_function(passed_b)



if __name__ == "__main__":
  start_http_server(8000)
  while True:
    pass
