import unittest
from unittest.mock import patch

from src.main import collect_users, get_data_projects, passed_b

class TestExporter(unittest.TestCase):

    def test_get_data_projects(self):
        mock_return = [{'number_of_projects': 'Number_Of_Projects'}]
        with patch('requests.get') as mock:
            mock.return_value.json.return_value = mock_return
            entity = get_data_projects()
            feedback = entity
            assert feedback == 1

    def test_collect_users(self):

        """Testing collect_s with mocked result"""

        mock_return = [{'number_of_users': 'Number_Of_Users'}]
        with patch('requests.get') as mock:
            mock.return_value.json.return_value = mock_return
            entity = collect_users()
            feedback = entity
            assert feedback == 1