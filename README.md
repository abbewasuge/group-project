# Group-project


## Getting started

### start the DevOps environment

```
git clone https://gitlab.com/abbewasuge/group-project.git
```
Then run the Docker 

```
docker compose up -d
```

### In case of https error

Get the ip address for docker self hosted 

```
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' self-hosted-container-name
```

change or create the ip address in daemon.json 

restart the docker and container service in Linux

```
systemctl restart docker.service
systemctl restart containerd.service 
```
Then start compose again 

### Onedev 


Create a new project in Onedev

![image.png](./screenshot/image.png)


Add a job executor for server docker executor

![image-1.png](./screenshot/image-1.png) 

![image-2.png](./screenshot/image-2.png)

Create .onedev-buildspec.yml in OneDev

Add a checkout code step with parameters

![image-3.png](./screenshot/image-3.png)

Add push and build step with parameters

![image-4.png](./screenshot/image-4.png) 

After that run the pipeline and the image should pushed to docker self host registry 

To check if image in docker repository open 

[]:port/v2/_catalog 


## Run the exporter from docker 

Make sure the image is built in docker self-hosted 

Get the IP address of onedev to use as a parameter in docker
```
docker run -e ip=$ip --publish 8000:8000 localhost:5000/exporter
```


Change the exporter's IP address in prometheus.yml to the your exporter ip address


## Connect grafana to prometheus

1- Click on the "cogwheel" in the sidebar to open the Configuration menu.

2- Click on "Data Sources".

3- Click on "Add data source".

![image-1.png](./screenshot/Screenshot_from_2022-09-26_10-12-39.png) 

4- Select "Prometheus" as the type.
5- Set the appropriate Prometheus server URL (http://localhost:9090/)

![image-2.png](./screenshot/Screenshot_from_2022-09-26_10-12-54.png) 

6- Click "Save & Test" to save the new data source.

![image-3.png](./screenshot/Screenshot_from_2022-09-26_10-15-01.png) 

7- Create a new dashboard in grafana 

![image-4.png](./screenshot/Screenshot_from_2022-10-09_16-07-32.png) 

8- Write the name of the exporter in metric 

![image-5.png](./screenshot/Screenshot_from_2022-10-09_16-19-02.png) 

The final result will look like this

![image-6.png](./screenshot/Screenshot_from_2022-10-09_16-13-58.png) 

![image-7.png](./screenshot/Screenshot_from_2022-10-09_16-19-45.png) 




